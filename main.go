// Package main implements a client for Greeter service.
package main

import (
	"context"
	"flag"
	"log"
	"os"

	pb "adesigner.in/ashish/client-app/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
	"google.golang.org/protobuf/types/known/emptypb"
)

const (
	defaultName = "world"
)

var (
	name = flag.String("name", defaultName, "Name to greet")
)

func main() {
	flag.Parse()
	addr, grpcServer := os.LookupEnv("GRPC_SERVER")
	if grpcServer == false {
		log.Fatal("GRPC_SERVER Required to initiate GRPC server `GRPC_SERVER`")
	}

	// Set up a connection to the server.
	conn, err := grpc.Dial(addr, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	c := pb.NewAccountServiceClient(conn)

	// Contact the server and print out its response.

	ctx := context.Background()

	md := metadata.New(map[string]string{
		"Authorization": "Secret",
	})
	ctx = metadata.NewOutgoingContext(ctx, md)
	r, err := c.Get(ctx, &pb.GetAccountRequest{})
	if err != nil {
		log.Fatalf("could not greet: %v", err)
	}
	log.Printf("Account Service: %s", r.GetMessage())

	rPeople, err := c.GetAllPerson(ctx, &emptypb.Empty{})

	log.Print(rPeople)
}
